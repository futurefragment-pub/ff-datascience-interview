# A* Algorithm
Your task is to complete the AStarSolver.py in order to determine the shortest path between two points provided as tuples
You may make use of libraries such as numpy

## Bonus
- If you are able to use matplotlib to plot the result of the algorithm
- If you are able to use the relevant distance metric depending on the "allow_diagonals" flag

## Examples
The following images show the results of the first two test cases with "allow_diagonals" set to true:

![Alt text](../assets/astar.png?raw=true "a star example 1")
![Alt text](../assets/astar2.png?raw=true "a star example 2")