from .AStarSolver import AStarSolver

TestGrid1 = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0],
             [0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0],
             [0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0],
             [0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0],
             [0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0],
             [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
             [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
             [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

TestGrid2 = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0],
             [0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0],
             [0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0],
             [0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0],
             [0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0],
             [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
             [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
             [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
             [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

TestGrid3 = [[0, 0, 0, 0, 0, 0, 0, 0, 0],
             [0, 1, 1, 1, 1, 1, 1, 1, 0],
             [0, 0, 0, 0, 0, 1, 0, 1, 0],
             [0, 0, 0, 1, 0, 1, 0, 1, 0],
             [0, 0, 1, 0, 1, 1, 0, 1, 0],
             [0, 1, 0, 1, 1, 1, 0, 1, 0],
             [1, 0, 1, 1, 1, 1, 0, 1, 0],
             [0, 0, 0, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, 0, 0, 0, 0]]

if __name__ == "__main__":
    map_1 = AStarSolver(TestGrid1)
    print(map_1((0, 0), (9, 9), True))
    print(map_1((0, 0), (9, 9), False))
    map_2 = AStarSolver(TestGrid1)
    print(map_2((0, 0), (9, 9), True))
    print(map_2((0, 0), (9, 9), False))
    map_3 = AStarSolver(TestGrid1)
    print(map_3((0, 0), (9, 9), True))
    print(map_3((0, 0), (9, 9), False))
