class AStarSolver:
    def __init__(self, map):
        """

        :param map: 2D array containing 0's for movable areas and other characters for walls
        """
        # TODO parse map here
        pass

    def __call__(self, start, goal, allow_diagonals=False):
        """

        :param start: tuple (x,y) of start position
        :param goal: tuple (x,y) of desired end position
        :param allow_diagonals: A flag stating if diagonal moves are allowed
        :return: Array of characters indicating the final moves.
        when diagonals are not allowed the algorithm will return 'W', 'A', 'S', 'D' for up, left, down and right
        and additionally 'Q', 'E', 'Z', 'C' for the top left, top right, bottom left and bottom right diagonals
        respectively if diagonals are allowed.
        """
        # TODO calculate solution path here
        pass
