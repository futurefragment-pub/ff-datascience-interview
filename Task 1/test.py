import unittest
from .CountPatterns import count_patterns_from


class CountPatterns(unittest.TestCase):

    def test_all(self):
        self.assertEqual(count_patterns_from('A', 10), 0)
        self.assertEqual(count_patterns_from('A', 0),  0)
        self.assertEqual(count_patterns_from('E', 14), 0)
        self.assertEqual(count_patterns_from('B', 1),  1)
        self.assertEqual(count_patterns_from('C', 2),  5)
        self.assertEqual(count_patterns_from('E', 2),  8)
        self.assertEqual(count_patterns_from('E', 4),  256)


if __name__ == '__main__':
    unittest.main()
