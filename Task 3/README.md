# Task 3: Machine Learning Skills
Lego is a construction toy consisting of plastic interlocking building blocks. There are hundreds if not thousands of different types of Lego bricks available allowing you to consturct an almost infinite number of different things.

The task at hand is to classify a subset of these types of bricks/blocks. Provided is the Kaggle dataset[1] “Images of Lego Bricks” that has approximately 12700 images with 16 different classes of Lego bricks. 

## Permissable Frameworks:
- Tensorflow (you may use Keras, but only with TF backend)
- PyTorch

## Tips:
- Data Preprocessing is strongly advised. 
- Do a confusion matrix and/or make use of other scoring systems
- Produce some graphs, you are not explicity required to use TensorBoard.