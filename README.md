<img src="assets/ff_logo.png" alt="Future Fragment" width="200"/>

# Data Science @FutureFragment
A challenge a day will result in a new job by May. (Joke relevant at time of challenge creation)

Here be’ith one of such challenges.  

All tasks need to be committed to a Git repo, be it Github, GitLab or Bitbucket. 

You have 7 days to complete this task. Apon completeion please send through your model artifcats, log files and any other neccicary files, along with the git repo location to imran@futurefragment.com

## References
[1] https://www.kaggle.com/joosthazelzet/lego-brick-images

Note: Please refrain from submitting verbatum copies of other peoples work.
___
Copyright Future Fragment 2019 ©